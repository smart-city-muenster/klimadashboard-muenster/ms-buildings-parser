Since November 2023 Data has changed.

**New Data Sample**:
```
"Datum";"Stadtb�cherei (0022),Strom (S~00011545),[kWh]";"* Ersatzwert"
"2017-01";"33546,375";" "
"2017-02";"29976,875";" "
"2017-03";"33206,000";"*"
"2017-04";"28044,875";" "
"2017-05";"29256,250";" "
"2017-06";"27876,500";" "
"2017-07";"29812,375";" "
"2017-08";"31064,375";" "
"2017-09";"30056,875";" "
"2017-10";"29497,250";" "
"2017-11";"31479,250";" "
"2017-12";"30602,750";" "
```

**Old Data Sample**:
```
"Stromverbrauch pro Monat  im Jahr 2022"
"f�r die Messstelle Strom"

"Datum","Stadtb�cherei (0022)"," "
,"Strom (S~00011545)",""
,"[kWh]",""
"Jan. 2022","25129,375"," "
"Feb. 2022","23685,125"," "
"M�rz 2022","25792,000"," "
"Apr. 2022","21997,625"," "
"Mai 2022","22230,125"," "
"Juni 2022","21514,875"," "
"Juli 2022","21965,625"," "
"Aug. 2022","22846,750"," "
"Sep. 2022","21879,875"," "
"Okt. 2022","21619,500"," "
"Nov. 2022","21471,125"," "
"Dez. 2022","20062,500"," "
"Summe","270194,500"," "

```

Issues:
- Incorrect format, not machine-readable by default.
- Data starts after a few rows.
- Start of data is different for different buildings (e.g., "Stadtbuecherei" and "Sentruper").
- Columns are not named correctly, more columns in header than in the data itself
- Date field is inconsistent (e.g., "Jan. 2022" and "M�rz 2022", not parsable by default).
- "Summe" field does not make sense.