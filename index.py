import requests
from io import StringIO
import pandas as pd
import urllib.parse
# import locale
import datetime
from functools import reduce
import sys
from pathlib import Path

# The start and end year for the data
START_YEAR = 2017
# end year is current year
END_YEAR = datetime.datetime.now().year

BUILDINGS = ["stadtbuecherei", "sentruper", "rathaus", "freiherr-von-stein"]


# The base URL for the API
BASE_URL = 'https://www.eview.de/smartoptimo/p3Export.php/?'

# The query parameters for the API
strom = {
    "stadtbuecherei": "0022;S~00011545",
    "sentruper": "0148;S~00001627",
    "rathaus": "0626;S~00011570",
    "freiherr-von-stein": "0985;000000\_3",
}


waerme = {
    "stadtbuecherei": "0022;f~00011543",
    "sentruper": "0148;000000\_7",
    "rathaus": "0626;f~00011568",
    "freiherr-von-stein": "0985;f~00001594",
}

# Get the URL for the API
def get_url(id, year):
    query_params = {
        "frame": "StadtMS",
        "p": create_p_query_param(id, year)
    }
    return BASE_URL + urllib.parse.urlencode(query_params)


# Create the query parameter for the API
def create_p_query_param(id, year, sep=","):
    return '{id};dgx804;t{year};dez={sep};'.format(id=id, year=year, sep=sep)

# Download the CSV data and decode it


# Download the CSV data and decode it
def download_and_decode(url):
    response = requests.get(url)
    data = response.content.decode('iso-8859-1')

    # Replace non-breaking space character with regular space
    return data.replace(u'\xa0', u' ').replace('"', "")

# Format the date column


# Convert the "Datum" column to a datetime format
def format_date(df):
    # set the German locale
    # locale.setlocale(locale.LC_TIME, 'de_DE.UTF-8')

    # Convert the "Datum" column to a datetime format. Date column values look like this:  2017-08
    df['Datum'] = df['Datum'].apply(lambda x: datetime.datetime.strptime(str(x), "%Y-%m"))

    return df


# Download the CSV data and convert it to a DataFrame
def url_to_df(url, id="ID"):
    data = download_and_decode(url)

    # Read the CSV data into a Pandas DataFrame
    df = pd.read_csv(StringIO(data), delimiter=';', decimal=",",  engine='python')

    # Rename the columns
    df.columns = ['Datum', id, "DELETE_ME"]

    # Convert the values to numeric
    df[id] = pd.to_numeric(df[id], errors='coerce')

    # remove unused column
    df = df.drop('DELETE_ME', axis=1)

    df = format_date(df)

    # Reset the index
    df = df.reset_index(drop=True)
    return df


# Create a DataFrame for each building and merge them
def create_df_for_energy_type(type, debug_type):

    # Create an empty list to store the DataFrames
    dfs = []

    # Iterate over all ids and years and create a DataFrame for each
    for id in BUILDINGS:
        place_dfs = []
        years = list(range(START_YEAR, END_YEAR + 1))
        for year in years:
            print("[{debug_type}] Processing {id} for year {year}".format(
                debug_type=debug_type, id=id, year=year))
            my_url = get_url(type[id], year)
            my_data = url_to_df(my_url, id)
            place_dfs.append(my_data)

        # Concatenate the DataFrames and append them to the list
        dfs.append(pd.concat(place_dfs))

    # Merge the DataFrames
    merged_df = reduce(lambda left, right: pd.merge(
        left, right, on='Datum', how="outer"), dfs)
    merged_df.reset_index(drop=True)
    return merged_df


# Write the DataFrame to a JSON file
def write_file(df, file_name):
    folder_path = sys.argv[1]
    p = Path(folder_path)
    p.mkdir(exist_ok=True)
    with open(p / file_name, 'w') as f:
        f.write(df.to_json(orient="records"))
        print("Wrote file {file_path}".format(file_path=p / file_name))


strom_df = create_df_for_energy_type(strom, "strom")
waerme_df = create_df_for_energy_type(waerme, "waerme")

write_file(strom_df, "strom.json")
write_file(waerme_df, "waerme.json")
