# Energy Consumption Data

This Python project fetches energy consumption data for the city of Münster from the SmartOptimo API and processes it into a JSON format. It currently supports data from the years 2017 to the current year.

## Installation

1. Clone the repository
2. Install the required packages with `pip install -r requirements.txt`

## Usage

To use this project, run `python index.py <output_folder>` from the command line, where `<output_folder>` is the folder where you want to save the JSON files.
