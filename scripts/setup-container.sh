#!/bin/bash

sudo sed -i 's/^# *\(de_DE.UTF-8\)/\1/' /etc/locale.gen
sudo locale-gen
pip3 install --user -r requirements.txt